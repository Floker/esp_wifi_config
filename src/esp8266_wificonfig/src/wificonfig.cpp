#include "wificonfig.h"


AsyncWebServer WiFiConfigserver(80);

WiFiConfig::WiFiConfig()
{
    //server = p_server;
    //
    ip.fromString("192.168.1.200");
    apIP.fromString("192.168.1.184");
    gateway.fromString("192.168.1.1");
    subnet.fromString("255.255.255.0");
    //Start LittleFS
    if(!LittleFS.begin()){
        Serial.println("An Error has occurred while mounting LittleFS");
    }
}

void WiFiConfig::init()
{   
    //TEST
    writeMemory(0,"545454");
    writeMemory(50,"gol575789");
    
    getSSIDNames();
    (*this).getSavedNetworkData((*this).ssid,(*this).pass);

    if (SSID_list.indexOf((*this).ssid) >= 0)
    {
        Serial.println((*this).ssid);
        Serial.println((*this).pass);

        connectWiFi();
    }
    else
    {
    startConfig();
    }

    //Serial.print(result);
}

int8_t WiFiConfig::scanNetworks(String * list)
{
  String result = "";
  String signalQuality = "";
  String securityType = "";
  int8_t nNetworks = WiFi.scanNetworks();
  SSID_count = nNetworks;
  Serial.println("escaneo terminado");
  if (nNetworks == 0) {
    Serial.println("no se encontraron redes");
    result = "NO_NETWORKS_FOUND";
  }  
  else
  {
    Serial.print(nNetworks);
    Serial.println(" redes encontradas");
    result = "";
    for (int i = 0; i < nNetworks; ++i)
    {
        int32_t signal = WiFi.RSSI(i);
        if     (signal > -79)
            signalQuality = "High";
        else if(signal > -88)
            signalQuality = "Good";
        else if(signal > -95)
            signalQuality = "Regular";
        else
            signalQuality = "Bad";

        uint8_t secType = WiFi.encryptionType(i) ;
        switch(secType){
        case 5:  
                securityType = "WEP";
        case 2:
                securityType = "WPA/PSK";
        case 4:
                securityType = "WPA2/PSK";
        case 7:
                securityType = "OPEN";
        case 8:
                securityType = "WPA/WPA2/PSK";
        }

      result = (result) + WiFi.SSID(i) + "|" + signalQuality + "|" + securityType + ";";
      delay(10);
    }
  }
  *list = result;
  return nNetworks;
}

void WiFiConfig::getSSIDNames()
{
    SSID_list = "";
    int nNetworks = scanNetworks(&SSID_list);
    String netwoks[nNetworks];
    for(int i = 0; i < nNetworks; i++)
    {
        String token = SSID_list.substring(0, SSID_list.indexOf(';'));
        SSID_list = SSID_list.substring(SSID_list.indexOf(';') + 1);
        netwoks[i] = token.substring(0, token.indexOf('|'));
    }
    SSID_list = "";
    for(int i = 0; i < nNetworks; i++)
    {
         SSID_list = (SSID_list) + netwoks[i] + "|";
    }
}

void WiFiConfig::readMemory(const int addr, String * payload)
{
    byte lectura;
    String temp_payload;
    for (int i = addr; i < addr+50; i++) {
        lectura = EEPROM.read(i);
        if (lectura != 255) {
            temp_payload += (char)lectura;
        }
    }
    *payload = temp_payload;
}

void WiFiConfig::writeMemory(const int addr, const String payload)
{
    int len = payload.length(); 
    char inchar[50]; 
    payload.toCharArray(inchar, len+1);
    for (int i = 0; i < len; i++) {
        EEPROM.write(addr+i, inchar[i]);
    }
    for (int i = len; i < 50; i++) {
        EEPROM.write(addr+i, 255);
    }
    EEPROM.commit();
}

void WiFiConfig::getSavedNetworkData(char * p_ssid, char * p_pass)
{   String temp_ssid;
    String temp_pass;
    (*this).readMemory(0 , &temp_ssid);
    (*this).readMemory(50 , &temp_pass);
    temp_ssid.toCharArray(p_ssid, 50);
    temp_pass.toCharArray(p_pass, 50);
}

void WiFiConfig::saveConfig()
{
    writeMemory(0,String((*this).ssid));
    writeMemory(50,String((*this).pass));
}

void WiFiConfig::connectWiFi()
{
    WiFi.mode(WIFI_STA); //para que no inicie el SoftAP en el modo normal
    WiFi.begin((*this).ssid, (*this).pass);
    int contConn = 0;
    while (WiFi.status() != WL_CONNECTED and contConn <50) { //Cuenta hasta 50 si no se puede conectar lo cancela
        ++contConn;
        delay(250);
        Serial.print(".");
        digitalWrite(13, HIGH);
        delay(250);
        digitalWrite(13, LOW);
    }
    if (contConn <50) {   
        Serial.println("");
        Serial.println("WiFi conectado");
        Serial.println(WiFi.localIP());
        digitalWrite(13, HIGH);  
    }
    else { 
        Serial.println("");
        Serial.println("Error de conexion");
        digitalWrite(13, LOW);
    }
}

// Replaces placeholder with LED state value
String WiFiConfig::RESTprocessor(const String& var){
  Serial.println(var);
  String package;
  //<option value="audi">Audi</option>
  if(var == "WIFILIST")
  {
    String token;
    String ssids_aux = SSID_list;
    for(int i = 0; i < SSID_count; i++)
    {
        String token = ssids_aux.substring(0, ssids_aux.indexOf('|'));
        ssids_aux = ssids_aux.substring(ssids_aux.indexOf('|') + 1);
        package = package + "<option value='" + token + "'>" + token + "</option>";
    }
    
  }
  Serial.println(package);
  return package;
}

void WiFiConfig::startConfig()
{
    WiFi.mode(WIFI_AP_STA);
    if (!WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0))) {
    Serial.println("STA Failed to configure");
    }
    WiFi.softAP((*this).ssidAP, (*this).passAP);
    IPAddress myIP = WiFi.softAPIP();
    delay(100);
    Serial.print("IP del acces point: ");
    Serial.println(myIP);
    Serial.println("WebServer iniciado...");
    startConfigServer();
}


void WiFiConfig::startConfigServer(){
    // Route for root / web page
    WiFiConfigserver.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/index.html", String(), false, RESTprocessor);
    });
    
    // Route to load style.css file
    WiFiConfigserver.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/style.css", "text/css");
    });

    // Route to load style.css file
    //WiFiConfigserver.on("/save_conf", HTTP_GET, [](AsyncWebServerRequest *request){
    //    request->send(LittleFS, "/style.css", "text/css"); //modificar esto
    //});

    WiFiConfigserver.on("/save_conf", HTTP_GET, [](AsyncWebServerRequest *request){
 
    int paramsNr = request->params();
    Serial.println(paramsNr);
 
    for(int i=0;i<paramsNr;i++){
 
        AsyncWebParameter* p = request->getParam(i);
        Serial.print("Param name: ");
        Serial.println(p->name());
        Serial.print("Param value: ");
        Serial.println(p->value());
        Serial.println("------");
    }
    
 
    request->send(200, "text/html", "<!DOCTYPE html><html><label for='networks' style='font-family: Roboto, sans-serif; font-size: 12px; color: #FFF; text-align: center; display: block;'>Connected Successfully</label></html>");
  });

    

    //WiFiConfigserver->on("/guardar_conf", guardar_conf); //Graba en la eeprom la configuracion

    //WiFiConfigserver->on("/escanear", escanear); //Escanean las redes wifi disponibles
    
    WiFiConfigserver.begin();

}