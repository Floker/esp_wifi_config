#include <Arduino.h>
#include <ESP8266WiFi.h>
//#include <ESP8266WebServer.h>
#include <EEPROM.h>

#include "wificonfig.h"


//-------------------VARIABLES GLOBALES--------------------------
int contconexion = 0;
unsigned long previousMillis = 0;

//String mensaje = "";

//--------------------------------------------------------------
//WiFiClient espClient;
//ESP8266WebServer server(80);

WiFiConfig config;



//--------------------------------------------------------------


//------------------------SETUP-----------------------------
void setup() {
  
  // Inicia Serial
  Serial.begin(115200);
  Serial.println("");

  EEPROM.begin(512);

  config.init();

  //leer(0).toCharArray(ssid, 50);
  //leer(50).toCharArray(pass, 50);

  //setup_wifi();
}

//--------------------------LOOP--------------------------------
void loop() {

  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= 5000) { //envia la temperatura cada 5 segundos
    previousMillis = currentMillis;
    Serial.println("Funcionando...");
  }
}