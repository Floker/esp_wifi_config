#ifndef WIFICONFIG_h
#define WIFICONFIG_h

#include "Arduino.h"

//#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include <LittleFS.h>

#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>



struct networkData
{
    String networkName;
    String signalQuality;
    String securityType;
};

static String SSID_list;
static int8_t SSID_count;

class WiFiConfig
{
  private:
    
    char ssid[50];      
    char pass[50];
    char ssidAP[50] = "wifipruebaa";      
    char passAP[50] = "123456789";
    void readMemory(const int addr, String * payload);
    void writeMemory(const int addr, const String payload);
    void getSavedNetworkData(char p_ssid[], char p_pass[]);
    void saveConfig();
    void connectWiFi();
    
    void startConfigServer();
    static void paginaconf();
    IPAddress ip;
    IPAddress apIP;
    IPAddress gateway;
    IPAddress subnet;
    static String RESTprocessor(const String& var);
    


    

  public:
    WiFiConfig();
    void init();
    void startConfig();
    int8_t scanNetworks(String * list);
    void getSSIDNames();
    
    

};
#endif
